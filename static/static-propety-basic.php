<?php

// TODO: static property is a property belonging to a class so that to access it we do not need to instantiate an object, but directly call it by writing the name of the class.


class Product
{
    public static $total_product = 100;
}

class Blender extends Product
{
}
echo Product::$total_product;
echo '</br>';
echo Blender::$total_product;
