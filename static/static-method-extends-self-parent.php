<?php

// TODO: static properties and methods can be inherited and accessed inside normal methods


class Product
{
    private static $total_product = 100;

    public static function check_product()
    {
        return 'Total product : '.self::$total_product;
    }
}

class Blender extends Product
{
    public function check_blender()
    {
        return self::check_product().', termasuk 3 jenis Blender'.'</br>';
    }
}

class HairDryer extends Product
{
    public function check_hair_dryer()
    {
        return Product::check_product().', termasuk 3 jenis Hair Dryer'.'</br>';
    }
}

class Mixer extends Product
{
    public function check_mixer()
    {
        return parent::check_product().', termasuk 3 jenis Mixer'.'</br>';
    }
}

$product01 = new Blender();
echo $product01->check_blender();

$product02 = new HairDryer();
echo $product02->check_hair_dryer();

$product03 = new Mixer();
echo $product03->check_mixer();
