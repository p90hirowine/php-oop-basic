<?php

// TODO: static method is a method belonging to a class so that to access it we do not need to instantiate an object, but directly call it by writing the name of the class.


class Product
{
    public static $total_product = 100;

    public static function check_product()
    {
        return 'Total Product : '.self::$total_product;
    }
}

echo Product::check_product();
