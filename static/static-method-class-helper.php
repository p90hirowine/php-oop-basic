<?php

// TODO: class helper

class ProductHelper
{
    public static function check_valid_sku($sku)
    {
        return preg_match('/^[A-Z]{3}[0-9]{3}$/', $sku);
    }

    public static function check_valid_brand($brand)
    {
        $all_brand = ['Samsung', 'Sony', 'LG', 'Philips', 'Sharp', 'Sanken'];

        return in_array($brand, $all_brand);
    }
}

if (ProductHelper::check_valid_sku('AAA545')) {
    echo 'sku AAA545 valid'.'</br>';
}

if (ProductHelper::check_valid_sku('AAa545')) {
    echo 'sku AAa545 valid'.'</br>';
}

if (ProductHelper::check_valid_brand('Sharp')) {
    echo 'Brand Sharp already exists'.'</br>';
}

if (ProductHelper::check_valid_brand('Samsung')) {
    echo 'Brand Samsung already exists'.'</br>';
}

if (ProductHelper::check_valid_brand('Asus')) {
    echo 'Brand Asus already exists'.'</br>';
}
