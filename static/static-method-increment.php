<?php

// TODO: static method increment


class Product
{
    public static $total_product = 0;

    public function __construct()
    {
        self::$total_product++;
        echo 'class Product has been created, total product : '.self::$total_product.'</br>';
    }
}

$product01 = new Product();
$product02 = new Product();
$product03 = new Product();
$product04 = new Product();
