<?php

// TODO: how to create inheritance definision

class Product
{
    public $id_product;
    public $stocks;
}

class Television extends Product
{
    public $screen_size;
}

class WashMachine extends Product
{
    public $storage;
}

class Speaker extends Product
{
    public $config;
}

$product01 = new Television();
$product02 = new WashMachine();
$product03 = new Speaker();

var_dump($product01);
var_dump($product02);
var_dump($product03);
