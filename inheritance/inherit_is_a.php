<?php

// TODO: to check if a class is an instance of another class

class Product
{
    public $id_product;
    public $stocks;
}

class Television extends Product
{
    public $screen_size;
}

class WashMachine extends Product
{
    public $storage;
}

class Speaker extends Product
{
    public $config;
}

$product01 = new Television();
$product02 = new WashMachine();
$product03 = new Speaker();

var_dump(is_a($product01, 'Product'));
var_dump(is_a($product01, 'Television'));
var_dump(is_a($product02, 'Product'));
var_dump(is_a($product02, 'Television'));
var_dump(is_a($product03, 'Speaker'));
