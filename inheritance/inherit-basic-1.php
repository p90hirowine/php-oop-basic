<?php

// TODO: how to create inheritance definision

class Product
{
    public $brand = 'Sharp';
    public $stocks = 50;

    public function checkStocks()
    {
        return 'Stocks : '.$this->stocks;
    }
}

class Television extends Product
{
    public $general = 'Television';

    public function checkStocksTelevision()
    {
        return $this->general.' '.$this->brand.', '.$this->checkStocks();
    }
}

class TelevisionLCD extends Television
{
    public $type = 'LCD';

    public function checkStocksTelevisionLCD()
    {
        return $this->type.' '.$this->checkStocksTelevision();
    }
}

$product01 = new TelevisionLCD();
echo $product01 -> brand;
echo '</br>';
echo $product01 -> general;
echo '</br>';
echo $product01 -> type;
echo '</br>';
echo $product01 -> checkStocksTelevisionLCD();
