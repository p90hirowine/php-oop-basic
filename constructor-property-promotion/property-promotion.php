<?php

// TODO: this feature can be used to shorten the process of initializing properties which are usually written in constructors


class Product
{
    public function __construct(
        private $type,
        public $brand = 'Samsung',
        protected $stocks = 10
    ) {
        echo 'Product '.$this->type. ' has been created'.'</br>';
    }
}

$product01 = new Product('Television', 'Samsung', 20);
$product02 = new Product('Wash Machine');

var_dump($product01);
echo '</br>';
var_dump($product02);
