<?php

// TODO: how to access propety and methode in a class

class Product
{
    public $id = "001";
    public $brand = "Samsung";
    public $price = 4000000;

    public function orderProduct()
    {
        return 'Product ordered ...';
    }
}

$television = new Product();

// * ouput

echo $television->id;
echo '</br>';
echo $television->brand;
echo '</br>';
echo $television->price;
echo '</br>';
echo $television->orderProduct();
