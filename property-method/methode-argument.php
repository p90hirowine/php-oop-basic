<?php

// TODO: how to use argument in a methode class

// * class definision

class Product
{
    public $type = '';
    public $brand = '';
    public $stock = 0;

    public function orderProduct()
    {
        $this->stock -= 1;
    }

    public function ordersProduct($val)
    {
        $this->stock -= $val;
    }

    public function checkStock()
    {
        return 'Stock : '.$this->stock;
    }
}

// * object instantiation

$product01 = new Product();
$product01 -> type = 'Television';
$product01 -> brand = 'Samsung';
$product01 -> stock = 54;

// * outputs

echo $product01 -> checkStock();

echo '</br>';

$product01 -> ordersProduct(10);
echo $product01 -> checkStock();

echo '</br>';

$product01 -> ordersProduct(25);
echo $product01 -> checkStock();
