<?php

// TODO: methode exercise

// * class definision

class Product
{
    public $type = '';
    public $brand = '';
    public $stock = 0;

    public function orderProduct()
    {
        $this->stock -= 1;
    }

    public function ordersProduct($val = 10)
    {
        $this->stock -= $val;
    }

    public function addStocks($val = 12)
    {
        $endStocks = $this->stock + $val;
        $massage = '';

        if ($endStocks <= 100) {
            $this->stock = $endStocks;

            $massage = 'Stock Succesfully added'.'</br>';
            $massage .= 'Total stocks now is : '.$this->stock.'</br>';
        } else {
            $massage = 'Sorry, stock is full. Canceled add stocks'.'</br>';
            $massage .= 'Total stocks now is : '.$this->stock.'</br>';
        }

        return $massage;
    }

    public function checkStock()
    {
        return 'Stock : '.$this->stock.'</br>';
    }
}

// * object instantiation

$product01 = new Product();
$product01 -> type = 'Television';
$product01 -> brand = 'Samsung';
$product01 -> stock = 54;

// * outputs

echo $product01 -> checkStock();
echo '</br>';

echo $product01 -> addStocks();
echo '</br>';

echo $product01 -> addStocks(20);
echo '</br>';

echo $product01 -> addStocks(15);
echo '</br>';
