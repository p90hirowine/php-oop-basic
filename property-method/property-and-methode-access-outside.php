<?php

// TODO: how to override values propety and methode outside of class

class Product
{
    public $id = "001";
    public $brand = "Samsung";
    public $price = 4000000;

    public function orderProduct()
    {
        return 'Product ordered ...';
    }
}

$washMachine = new Product();
$washMachine -> id = '002';
$washMachine -> brand = 'LG';
$washMachine -> price = '1500000';

// * ouput

echo $washMachine->id;
echo '</br>';
echo $washMachine->brand;
echo '</br>';
echo $washMachine->price;
echo '</br>';
echo $washMachine->orderProduct();
