<?php

// TODO: how to create some objects in a class

class Product
{
    public $id = "001";
    public $brand = "Samsung";
    public $price = 4000000;

    public function orderProduct()
    {
        return 'Product ordered ...';
    }
}

// * television object

$television = new Product();
$television -> id = '001';
$television -> brand = 'Sharp';
$television -> price = 2000000;

// * wash machine object

$washMachine = new Product();
$washMachine -> id = '002';
$washMachine -> brand = 'LG';
$washMachine -> price = '1500000';

// * speaker object

$speaker = new Product();
$speaker -> id = '003';
$speaker -> brand = 'Logitech';
$speaker -> price = 950000;

// * ouput

print_r($television);
echo '</br>';
print_r($washMachine);
echo '</br>';
print_r($speaker);
