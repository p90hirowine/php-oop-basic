<?php

// TODO: setters and getters are names for methods that fill (set) and retrieve the value (get) of a property

class Product
{
    private $brand;
    private $stocks;

    private function set_brand($brand)
    {
        if (!is_string($brand)) {
            die('Error : Brand must be string !'.'</br>');
        } else {
            $this->brand = $brand;
        }
    }

    private function set_stocks($stocks)
    {
        if (!is_int($stocks)) {
            die('Error : Stocks must be integer !'.'</br>');
        } else {
            $this->stocks = $stocks;
        }
    }

    public function __construct($brand, $stocks)
    {
        $this->set_brand($brand);
        $this->set_stocks($stocks);
    }

    public function get_brand()
    {
        return strtoupper($this->brand);
    }

    public function get_stocks()
    {
        return strtoupper($this->stocks);
    }
}

$product01 = new Product('Acer', 10);
echo 'Stocks product '.$product01->get_brand().' : '.$product01->get_stocks();
