<?php

// TODO: getters & setters exercise


class Product
{
    private $ID_product;
    private $stocks;

    private function setID_product($ID_product)
    {
        if (!preg_match('/^[A-Z]{3}[0-9]{3}$/', $ID_product)) {
            die('Error : ID Product must consist of 6 digits (3 capital letters & 3 numbers) !');
        } else {
            $this->ID_product = $ID_product;
        }
    }

    private function setStocks($stocks)
    {
        if (is_int($stocks) && ($stocks > 0)) {
            $this->stocks = $stocks;
        } else {
            die('Error : Stocks must be positive integer !');
        }
    }

    public function __construct($ID_product, $stocks)
    {
        $this->setID_product($ID_product);
        $this->setStocks($stocks);
    }

    public function getID_product()
    {
        return $this->ID_product;
    }

    public function getStocks()
    {
        return $this->stocks;
    }
}

$product01 = new Product('LNV234', 10);

echo 'Stocks product '.$product01->getID_product().' : '.$product01->getStocks();
