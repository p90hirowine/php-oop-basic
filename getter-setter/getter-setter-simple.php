<?php

// TODO: setters and getters are names for methods that fill (set) and retrieve the value (get) of a property

class Product
{
    private $brand;
    private $stocks;

    public function set_brand($brand)
    {
        $this->brand = $brand;
    }

    public function set_stocks($stocks)
    {
        $this->stocks = $stocks;
    }

    public function get_brand()
    {
        return $this->brand;
    }

    public function get_stocks()
    {
        return $this->stocks;
    }
}

$product01 = new Product();
$product01->set_brand('Acer');
$product01->set_stocks(10);

echo $product01->get_brand();
echo '</br>';
echo $product01->get_stocks();
