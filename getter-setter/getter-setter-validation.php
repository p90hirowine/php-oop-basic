<?php

// TODO: setters and getters are names for methods that fill (set) and retrieve the value (get) of a property

class Product
{
    private $brand;
    private $stocks;

    public function set_brand($brand)
    {
        if (!is_string($brand)) {
            echo 'Error : Brand must be string !'.'</br>';
        } else {
            $this->brand = $brand;
        }
    }

    public function set_stocks($stocks)
    {
        if (!is_int($stocks)) {
            echo 'Error : Stocks must be integer !'.'</br>';
        } else {
            $this->stocks = $stocks;
        }
    }

    public function get_brand()
    {
        return strtoupper($this->brand);
    }

    public function get_stocks()
    {
        return strtoupper($this->stocks);
    }
}

$product01 = new Product();
$product01->set_brand('Acer');
$product01->set_stocks(10);

echo $product01->get_brand();
echo '</br>';
echo $product01->get_stocks();
