<?php

// TODO: learn meaning of this keyword

class Product
{
    public $type = '';
    public $brand = '';

    public function orderProduct()
    {
        return $this->type.' '.$this->brand.' ordered ...';
    }
}

// * objects instantiation

$product01 = new Product();
$product01 -> type = 'Television';
$product01 -> brand = 'Samsung';

$product02 = new Product();
$product02 -> type = 'Wash Machine';
$product02 -> brand = 'LG';

// * outputs

echo $product01 -> orderProduct();
echo '</br>';
echo $product02 -> orderProduct();
