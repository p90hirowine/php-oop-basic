<?php

// TODO: this exercise

// * class definision

class Product
{
    public $type = '';
    public $brand = '';
    public $stock = 0;

    public function orderProduct()
    {
        $this->stock -= 1;
    }

    public function checkStock()
    {
        return 'Stock : '.$this->stock;
    }
}

// * object instantiation

$product01 = new Product();
$product01 -> type = 'Television';
$product01 -> brand = 'Samsung';
$product01 -> stock = 54;

// * outputs

echo $product01 -> checkStock();

$product01 -> orderProduct();
$product01 -> orderProduct();
$product01 -> orderProduct();

echo '</br>';
echo $product01 -> checkStock();
