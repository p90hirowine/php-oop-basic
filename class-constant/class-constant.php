<?php

// TODO: class constant is a constant in the class.

class Product
{
    public $price_USD = 0;
    private const KURSUSD = 15000;

    public function price_IDR()
    {
        return $this->price_USD * self::KURSUSD;
    }
}

$product01 = new Product();
$product01->price_USD = 15;

echo $product01->price_IDR();
