<?php

// TODO: property overriding problem and solution
// * Solution: must use different property name

class Product
{
    public $brand = 'Sony';
}

class Television extends Product
{
    public $brand = 'Panasonic';
    public $brand_product = parent::brand;
}

$product01 = new Television();
echo $product01->brand_product;
