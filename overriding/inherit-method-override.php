<?php

// TODO: methode overriding problem and solution

class Product
{
    public function hello()
    {
        return 'this from Product';
    }
}


class Television extends Product
{
    public function hello()
    {
        return 'this from Television';
    }

    public function hello_product()
    {
        return parent::hello();
    }
}

$product01 = new Television();
echo $product01->hello_product();
