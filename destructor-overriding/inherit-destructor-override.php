<?php

// TODO: destructor override problem and solution

class Product
{
    public $type;
    public $brand;
    public $stocks;

    public function __construct($type, $brand, $stocks)
    {
        $this->type = $type;
        $this->brand = $brand;
        $this->stocks = $stocks;
    }

    public function __destruct()
    {
        unset(
            $this->type,
            $this->brand,
            $this->stocks
        );

        echo 'Class Product propertys has been deleted ...';
    }
}

class Television extends Product
{
    public $screen_size;

    public function __construct($type, $brand, $stocks, $screen_size)
    {
        parent::__construct($type, $brand, $stocks);
        $this->screen_size = $screen_size;
    }

    public function __destruct()
    {
        unset($this->screen_size);
        echo 'Class Television property has been deleted ...';
        parent::__destruct();
    }
}

$product01 = new Television('Television', 'Samsung', 20, 32);

var_dump($product01);
