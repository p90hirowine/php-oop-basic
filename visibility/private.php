<?php

// TODO: If a property or method is set as private, only the program code in that class can access it.

class Product
{
    private $brand;

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    public function getBrand()
    {
        return $this->brand;
    }
}

$product01 = new Product();
$product01->setBrand('Asus');

echo $product01->getBrand();
