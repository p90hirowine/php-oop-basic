<?php

// TODO: If a property or method is set as protected then program code outside the class cannot access it, unless it is an instance of that class.

class Product
{
    protected $brand = 'Asus';

    protected function hello()
    {
        return 'this is a product';
    }
}

$product01 = new Laptop();
echo $product01->hello_laptop();

class Laptop extends Product
{
    public function hello_laptop()
    {
        return $this->hello().' laptop '.$this->brand;
    }
}
