<?php

// TODO: If a property or method is set as public then all program code inside and outside the class can access it

class Product
{
    public $brand;

    public function hello()
    {
        return 'this from product';
    }
}

$product01 = new Product();
$product01->brand = 'Asus';

echo $product01->brand;
echo '</br>';
echo $product01->hello();
