<?php

// TODO: property cannot access final keyword

class Product
{
    public function hello()
    {
        return 'this from product';
    }
}

class Television extends Product
{
    final public $screen_size;
}
