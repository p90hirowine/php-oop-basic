<?php

// TODO: final keyword is used to break overriding and inheritance

class Product
{
    final public function hello()
    {
        return 'This from product';
    }
}

class Television extends Product
{
    public function hello()
    {
        return 'This from television';
    }
}

$product01 = new Product();
echo $product01->hello();
