<?php

// TODO: final keyword is used to break overriding and inheritance

final class Product
{
    public function hello()
    {
        return 'this from product';
    }
}

class Television extends Product
{
}
