<?php

// TODO: resolve a example problem with constructor methode

class Product
{
    public $type;
    public $brand;
    public $stocks;

    public function __construct($type, $brand, $stocks = 10)
    {
        $this->type = $type;
        $this->brand = $brand;
        $this->stocks = $stocks;
    }
}

$product01 = new Product('Televison', 'Samsung', 20);
$product02 = new Product('Wash Machine', 'LG');

print_r($product01);
echo '</br>';
print_r($product02);
