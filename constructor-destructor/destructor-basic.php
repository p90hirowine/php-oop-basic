<?php

// TODO: how to create a destructor methode in class

class Product
{
    public function __construct()
    {
        echo 'Constructor is running ...'.'</br>';
    }

    public function __destruct()
    {
        echo 'Destructor is running ...'.'</br>';
    }
}

$product01 = new Product();
$produk02 = new Product();
