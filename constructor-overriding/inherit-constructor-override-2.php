<?php

// TODO: constructor override problem and solution

class Product
{
    public $type;
    public $brand;
    public $stocks;

    public function __construct($type, $brand, $stocks)
    {
        $this->type = $type;
        $this->brand = $brand;
        $this->stocks = $stocks;
    }
}

class Television extends Product
{
    public $screen_size;

    public function __construct($type, $brand, $stocks, $screen_size)
    {
        parent::__construct($type, $brand, $stocks);
        $this->screen_size = $screen_size;
    }
}

$product01 = new Television('Television', 'Samsung', 20, 32);

print_r($product01);
