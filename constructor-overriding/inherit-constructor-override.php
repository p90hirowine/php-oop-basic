<?php

// TODO: constructor override problem and solution

class Product
{
    public $type;
    public $brand;
    public $stocks;

    public function __construct($type, $brand, $stocks)
    {
        $this->type = $type;
        $this->brand = $brand;
        $this->stocks = $stocks;
    }
}

class Television extends Product
{
    public function __construct($type, $brand, $stocks)
    {
        return parent::__construct($type, $brand, $stocks);
    }
}

$product01 = new Television('Television', 'Samsung', 20);

print_r($product01);
